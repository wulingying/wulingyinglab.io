---
title: 🏆 SegmentFault Hackathon 文艺复兴上海站作品集 - 获奖篇
date: 2016-05-24 16:21:15
tags:
  - hackathon
---

承[上回](https://segmentfault.com/a/1190000005174175)，在 Day 1 中，石墨文档、Agora.io 声网和 HPE 的技术团队针对自家的 SDK、API 等作了深度介绍，今天就来看看我们的开发者是如何将作品和服务关联起来的吧。

<!--more-->


## 跳蚤市场

24 小时的编程，艺术家都将什么作品搬上自己的摊位？如果你也和我一样，有点好奇，那就往下翻♪(^∇^*)

![](https://ssl.daoapp.io/ww2.sinaimg.cn/mw690/795bf814gw1f46jif8o6sj21kw16ogws.jpg)  
_跳蚤市场地图_

这次 Hackathon 的作品展示环节，我们采用“跳蚤市场”模式，即开发者所在座位就是「摊位」，大家在“摊位”上向围观的观众和评委介绍自己的作品。评委从影响力、创新性、设计和执行程度这 4 个方面选出本次 hackathon 前三名，开发者和观众用自己的投票符选出现场最具人气的作品，并送上神秘礼物。

## 获奖团队&作品

### Top 1

![](https://ssl.daoapp.io/ww1.sinaimg.cn/mw690/795bf814gw1f46ji80c4qj21kw11x1h9.jpg)  
_Top 1：New Bee，作品：Debattle - 一款基于鼓励自我思考的线上辩论平台_

随着奇葩说的火热，「爱上人工智能是爱情吗？」、「上司该列为发展对象吗？」等很多话题也成为大家茶余饭后探讨的内容。辩论，不再局限于某个特定团队，不再局限于校园的辩论赛，而是逐渐生活化。Debattle，面向辩论爱好者的作品，提供单人和组队模式，通过视频聊天即可在线上进行辩论。他们强调，`辩论不是为了辨明是非，而是鼓励自我思考，打破认知的边界！

`小编透露：Top 1 团队还掳走了石墨单项 & agora单项，小编正催着他们来发表一些技术层面上的作品介绍😊` 

### Top 2

![](https://segmentfault.com/img/bVww3H)  
_Top 2：CodeVS，作品：ChatX - 和与自身的业务逻辑完全解耦，开箱即用的聊天插件_

![](https://segmentfault.com/img/bVww7k)  
`在这个世界上，没有一句 JS 解决不了的事情（~~ 如果有，再来一句 JS ~~）` 。处于计算机变革的“文艺复兴”时代，他们重新定义 online communication。任何网站，只需加入一句 js，即可添加 ChatV，并获得私信、群聊、视频语音通话、客服支持等功能**

### Top 3

![](https://ssl.daoapp.io/ww4.sinaimg.cn/mw690/795bf814gw1f46jiak9yrj21kw11xnlg.jpg)  
_Top 3：不坑不捞，作品：Chalk War - 一款运用粉笔画风绘制的真人实时对战游戏_

通过 ASUS Xtion PRO LIVE 采集数据，并使用基于 OpenNI 1.5 & NiTE 1.5 的骨架追踪算法标识人体特征点以识别人体不同动作。不同动作触发不同攻击效果，一方血槽清空则败。此外，不同的语音口令、手势组合还会触发特殊效果。各类效果通过 OpenCV 渲染至主画面，可以给玩家、观战者带来独特的游戏体验。


`小编说：Chalk War 粉笔战争还被现场观众和开发团队选为当场的人气奖，每个人抱走一个 Ruff 开发板（开撸了么？` 

### HPE 单项奖

![](https://ssl.daoapp.io/ww1.sinaimg.cn/mw690/795bf814gw1f46mc7f9eej21kw11xkg7.jpg)  
_HPE 单项奖：Avalanche，作品：Avalanche - 一款具有文本笔录获取功能的视频聊天软件_

工作学习中，会议、头脑风暴还是小组讨论产生的文字笔录整理相当不易，但录音又不便查阅及存储；远程教学中，手记笔记非常不便，看录音或视频又耗时耗力，在这样的背景下我们的这款软件应运而生，目标是提供便捷的多人视频聊天客户端+便捷的文本笔录获取功能。

BTW，  
[SegmentFault Hackathon 文艺复兴上海站更多照片](http://pan.baidu.com/s/1geDzrDT)  
[SegmentFault Hackathon 文艺复兴，深圳、北京站报名进行中](https://segmentfault.com/hackathon-2016)
