---
title: 和 HTTPS 相关文章整理
date: 2016-03-29 16:21:15
tags:
  - https
---

> 三月，多加一个 s，可能多出一个 spring 啊


这两天疫苗事件沸沸扬扬，未来的男朋友可能刚打了一管，想想真是心酸。

but，生活还是要继续，买买买依旧不能停 ![](https://segmentfault.com/img/bVtMYh)

买买买，发现淘宝`https`了，京东`https`了，亚马逊`https`了……[HTTPS 时代全面到来，你准备好了吗？](https://segmentfault.com/a/1190000004656363)

<!-- more -->

如果没准备好也没关系，那就先来个[《HTTPS 科普扫盲帖》](https://segmentfault.com/a/1190000004523659) 热个身。

如果你依旧没看懂，再来个[《全站 HTTPS 来了》](https://segmentfault.com/a/1190000004199917) 加深印象。

两遍之后，差不多可以[《升级 https, Let's Encrypt》](https://segmentfault.com/a/1190000004457037) 或者 [《给网站戴上「安全套」》](https://segmentfault.com/a/1190000004665299)。

当然，全站 https 也会让你在开发环境搭建过程中遇到麻烦，[《记一次移动端开发环境调试》](https://segmentfault.com/a/1190000004600645) 可能会帮到你。

最后,推荐两篇收藏较高的文章以及一个 HTTPS 网站检测工具：

 * [《关于 Web 安全，99% 的网站都忽略了这些》](https://segmentfault.com/a/1190000003852910)
 * [《扒一扒 HTTPS 网站的内幕》](https://segmentfault.com/a/1190000003801450)
 * [SSL Labs]()
 
课外阅读

[《白话解释 对称加密算法 VS 非对称加密算法》](https://segmentfault.com/a/1190000004461428)
[《白话解释 OSI 模型，TLS/SSL 及 HTTPS》](https://segmentfault.com/a/1190000004467714)

`还有 https、安全相关问题、文章推荐，请在评论处附上`