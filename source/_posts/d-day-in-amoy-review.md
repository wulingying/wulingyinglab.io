---
title: 在一座文艺的城市举办一场 D-Day——厦门「前端场」回顾
date: 2016-05-30 16:21:15
tags:
  - segmentfault-d-day
---
## 开场前

![](https://segmentfault.com/img/bVxEcA)

[SegmentFault D-Day](https://segmentfault.com/d-day-2016) 第一次来厦门，在这座文艺的城市，将一群开发者聚集在一起交流，会是怎样一种体验？一起来感受下吧。

因为第一次来厦门举办活动，SegmentFault 的工作人员都有一丝担忧，会不会没有很多人参加这一次的技术分享？但想到有腾讯无线应用中心的@当耐特、百度前端工程师@羡辙、SegmentFault 前端工程师@integ-standby、阿里技术专家竹暄和美团前端工程师@掬一捧这五位讲师实打实的干货分享，前端早读课@情封的热情推荐，我们也就不那么畏惧了。

<!-- more -->

![](https://segmentfault.com/img/bVxEgO)  
_签到入场_

下午 13:00，小伙伴陆续到场。现场除了在软件园工作的小伙伴，还有不少厦大爱编程的同学，甚至还有一些早上从福州坐动车过来的小伙伴。13:25，现场 300+ 椅子都已坐满，不少迟了两分钟的同学，都只能站着听。

![](https://segmentfault.com/img/bVxEig)  
_早读君情封也站着听讲师分享_

13:30，分享正式开始。首先是 SegmentFault 主持人烧碱对 SegmentFault 的一些介绍。SegmentFault 是开发者社区，提供问答、文章、笔记等功能，也为开发者提供[招聘平台](https://segmentfault.com/jobs)。

而 [SegmentFault D-Day](https://segmentfault.com/d-day-2016) 则是由 SegmentFault 发起主办的系列技术沙⻰，为开发者打造更具价值的线下技术交流。

此外，烧碱还介绍了现场工作人员、志愿者、嘉宾讲师，并表达了对赞助商、场地支持方、前端早读课的感谢，以及对到现场的开发者表示欢迎。

## 技术分享

![](https://segmentfault.com/img/bVxEiI)  
_分享开始，小伙伴们都聚精会神得听讲_

![](https://segmentfault.com/img/bVxEiZ)  
_美团前端工程师@掬一捧_

首位分享嘉宾是是美团前端工程师@掬一捧，现场分享了用来标准化前端 UI 组件开发，同时也方便前端童鞋们统一管理、索引所有的组件，减少重复开发和维护成本的组件集线器，一起来听下[《美团前端组件中心介绍》](http://slides.com/solome/mt-components-hub#/)吧。

![](https://segmentfault.com/img/bVxEwv)  
_百度前端工程师@羡辙_

第二位讲师是 ECharts 背后的女孩@羡辙，对于可视化渲染部分的测试，ECharts 是怎么做的呢？且听@羡辙 为大家介绍[前端可视化的测试实践](http://zhangwenli.com/ppt/2016/05/09/visualization-test)。

![](https://segmentfault.com/img/bVxEwD)  
_SegmentFault 前端工程师@integ-standby_

如同一夜之间 A girl to a woman, 我们心爱的 jq, angular, backbone 变成了 Flux, Redux, Elm-lang，茶歇过后，第三位讲师——SF 前端工程师@integ-standby 带着大家领略后 React 时代，具体内容请点[这里](http://pan.baidu.com/s/1i58lT2X)。

![](https://segmentfault.com/img/bVxEiV)  
_阿里技术专家竹暄_

第四位嘉宾是阿里技术专家竹暄，带来了议题[《Node.js 服务化架构和服务治理》](http://pan.baidu.com/s/1nuT2obv)，内容如下：

*   服务化需求的产生

*   问题和解决方案

*   未来的挑战

*   QA

![](https://segmentfault.com/img/bVxEh8)  
_腾讯无线应用中心的@当耐特_

最后一位讲师是爱手游、爱篮球，自然科学、UFO 爱好者@当耐特 分别从Web图形技术概览、Canvas图形库设计、动画、富媒体跨屏适配方案四方面讲《Web 图形程序设计》，俗称网页变换特效，风趣幽默的讲师时不时将在座的小伙伴逗笑，PPT 请戳[这里](http://pan.baidu.com/s/1kVxYzPX)。

![](/image?src=http://ww1.sinaimg.cn/mw690/795bf814gw1f4dpic7fkmj21kw11xh4r.jpg&objectId=1190000005598413&token=6fcf4e1ea9c0dd4b5f67f01709be1cdd)  
_大合影：2016 年 5 月 28 日 于厦门_

嘉宾分享结束，在场的小伙伴聚在一起，合影留念，你们一定好奇为什么没有问答环节，题目啊，在每位嘉宾分享结束的瞬间，就直接提问回答咯。

## Q & A

#### 资料

Q：那么嘉宾分享的资料哪里能 get？  
A：here👇

*   李阳 -[《美团前端组件中心介绍》](http://slides.com/solome/mt-components-hub#/)

*   羡辙 -[《前端可视化的测试实践》](http://zhangwenli.com/ppt/2016/05/09/visualization-test)

*   竹暄 - [《Node.js服务化架构和服务治理》](http://pan.baidu.com/s/1nuT2obv)

*   张磊 - [《Web 图形程序设计》](http://pan.baidu.com/s/1kVxYzPX)

*   姜上 -[《后 React 时代的前端：这一切没有想象的那么糟》](http://pan.baidu.com/s/1i58lT2X)

#### 照片

Q：请问现场照片哪里可以看到？  
A：戳[这里](http://pan.baidu.com/s/1i4Khd21)。

#### 视频

Q：这场分享会有视频么？  
A：Of course，不过还在等待后期制作。

#### 活动

Q：请问还会有此类的活动么？  
A：还有哦，不过今年厦门就是这一场，我们还会为[西安](https://segmentfault.com/e/1160000004844727)、[广州](https://segmentfault.com/e/1160000004996226)、[大连](https://segmentfault.com/e/1160000005059284)的开发者带来技术沙龙。

## 写于后

![](https://segmentfault.com/img/bVxEbW)

这就是在一座充满文艺气息的城市举办 D-Day 的简单记录，谢谢厦门小伙伴的热情参与，看到不少小伙伴在朋友圈、微博发状态，莫名有种被肯定的感动，相信下次还会为大家带来更多的优质分享。