---
title: 直播系统的云构建干货分享
date: 2016-07-01 16:21:15
tags:
  - segmentfault
---
![](https://ssl.daoapp.io/ww4.sinaimg.cn/mw690/795bf814gw1f5a6nvt2wbj21kw11taxo.jpg)

看球赛需要直播，娱乐行业需要直播，教育行业需要直播，金融行业需要直播，直播大火的今天，你怎能不懂直播技术？就在上周六，SegmentFault 携手 UCloud 为大家带来了一场直播相关的技术沙龙——直播系统的云构建，现场讲师带来哪些技术分享？小编就为你一一道来。

![](https://ssl.daoapp.io/ww4.sinaimg.cn/mw690/795bf814gw1f5a6nmhm08j21kw11tkjf.jpg)

首位分享嘉宾是趣看直播创始人黑豹，为大家带来了议题[《“趣看直播+”，一键叫来一个亿的背后产品和技术》](http://pan.baidu.com/s/1nu85cAt)。黑豹从技术和产品两方面和大家分享了一些趣看做直播的经验，并将「实时转码，低延迟，高并发」三个关键点介绍给大家。

![](https://ssl.daoapp.io/ww3.sinaimg.cn/mw690/795bf814gw1f5a6o1tpv0j20m80eugr4.jpg)

没有互动的直播不叫直播，齐书创始人阚嘉基于「一个线上的万体馆（8-10 万人同时在线）规模的直播服务」和在场的小伙伴分享了[《百万同时在线的云直播互动系统》](http://pan.baidu.com/s/1nvGSMhV)，并聊了聊基本构建、消息流、多核系统、双向互动等多个技术点。

![](https://ssl.daoapp.io/ww1.sinaimg.cn/mw690/795bf814gw1f5a7rg2ekqj21kw11y4qp.jpg)

来自 UCloud 的解决方案架构师林超首先介绍了直播的基本要素和概念，接下来详细介绍了 UCloud 平台云的引进、直播的分发网络以及大量的文件存储等内容。议题[《直播云的架构演进与如何快速构建一个直播应用》](http://pan.baidu.com/s/1kUSlXwr)。

![](https://ssl.daoapp.io/ww3.sinaimg.cn/mw690/795bf814gw1f5a75lk3zgj21kw11te7p.jpg)

<!-- more -->


嘉宾分享完干货，接下来就是圆桌讨论环节，本次圆桌由 UCloud 讲师林超主持，三位讲师就以下三个问题进行了讨论：

*   未来直播的方向到底是什么样？

*   直播在 VR 这一块有什么难点？

*   直播在行业上的一些特点及属性

想了解嘉宾们对这些问题的看法，可以戳[这里](http://pan.baidu.com/s/1cFTsGA)。

伴随着圆桌结束，此次分享也到此结束。各种直播横空出世的当下，直播技术远不止今天技术沙龙上分享的这些，SegmentFault 和 UCloud 希望能为大家带来更多直播方面的技术分享。

本次活动的干货、速记稿、照片请至[直播系统的云构建分享区](http://pan.baidu.com/s/1b2fRp8)查看。
