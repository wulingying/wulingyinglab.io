---
title: Hackathon 文艺复兴北京站 Day 2
date: 2016-06-29 16:21:15
tags:
  - segmentfault
  - hackathon
---

1 位开发如何和 4 位产品经理和谐度过 24 小时？

Hackathon 文艺复兴北京站的一位程序员就很好地回答了这个问题。在 Demo 演示环节，他说这绝非最终产品，小编私心想着，最终的产品还是要经过和产品经理厮杀打磨才能诞生。

![](https://ssl.daoapp.io/ww2.sinaimg.cn/mw690/795bf814gw1f5c23guvkoj21kw11xk8a.jpg)  
_图为该团队顽强而幽默的程序员_

除了这件有趣的事，想知道本次 hacakthon 出了哪些好玩的作品，往下翻吧。

<!-- more -->


### 一等奖作品：Project M

第一名又是那个来捣乱的 hackathon 专业户 hACKbUSTE 团队，号称自己是 hack 渣渣，靠在 hackathon 上卖艺为生。同时，他们也是 agora 的单项奖获得者。唉，小编的祝贺词都要穷了。

![](https://ssl.daoapp.io/ww1.sinaimg.cn/mw690/795bf814gw1f5c23hsb83j20zk0nqwgk.jpg)  
_Project M 演示_

这次 hACKbUSTE 团队的项目名称为 Project M，M 指命题米开朗基罗。Project M 是个浏览器扩展程序，现场需使用 Agora.io 的 SDK，当我们使用浏览器看网页视频时，会被随机分配到聊天室，聊天室内是同时浏览这一网页的观众，通过聊天室，观众之间可以实时互动，评判、瞎侃～

### 二等奖作品：The Great Stealer

团队 Find the Lamp 由一对情侣和一位 logo 手绘外援组成。在演示 keynote 时，妹子说，“作为一名 iOS 工程师，还从来没画过这么多界面”，（= =妹子原来是实力参赛者，小编原以为是实力鼓励师）。

正是这完整的作品界面，该作品还同时获得石墨文档单项奖和观众投票出的 Ruff 人气奖。最后不得不提一下该团队的 slogan 是：走了那么远，我们去寻找一盏灯。

![](https://ssl.daoapp.io/ww3.sinaimg.cn/mw690/795bf814gw1f5c23iliw1j21kw11xdro.jpg)  
_作品：The Great Stealer_

Find the Lamp 这一次带来的项目是一款文艺复兴艺术品的收集游戏，名为 The Great Stealer。游戏的故事以小偷帮助美术馆长恢复名画为引子，意在通过将艺术品拆解和重组，去学习艺术知识。在游戏中，我们可以动手将名画，如米开朗基罗的《创世纪》中的男人、上帝、天使等元素抓下来，再重新组合成新的画，或者将上帝和天使元素组成《末日审判》。

### 三等奖作品：看看

第三名天行 Lamp 就是传说中一言不合就合并的团队，由原来的天行者和 Solid Lamp 合成。他们的项目名称是看看，比如对着 Anyone Can Code 的 T 恤拍摄，会显示出这次 hackathon 的信息。

![](https://ssl.daoapp.io/ww4.sinaimg.cn/mw690/795bf814gw1f5c23kidoqj21kw16o4hd.jpg)

作品通过前端 WebAPP 的摄像头进行图像采集，经过图像处理算法计算图像的相关特征，与数据库内已存在的图像特征进行匹配，获得匹配结果，然后在应用的屏幕标记出识别到的目标物体，并显示目标的其他相关信息。

### Wacom 单项奖：印迹北京

来自北大研一的几位小伙伴，产品狗、程序猿、设计狮，还有萌萌的程序员鼓励师组成了一个团结友爱的小队伍——Vanilla。

他们的项目印迹北京，是以北京文化为背景的你画我猜网页小应用，利用 wacom 的 WILL SDK 和微软认知服务，使用者可以即时将画布分享，一个小伙伴作画，另一个小伙伴通过语音说出画的对象，比如天坛，比如糖葫芦。此外，还会有相应的文化介绍，加深大家对北京的印象~

评比结束，SegmnetFault 北京站就结束了，SegmentFault Hackathon 文艺复兴也正式收尾了。有人很情怀地说道：“想想昨天的情景就是：一罐公牛，一屏代码，一夜无眠，一声叹息”。虽说 hackathon 伤身，然而不体验一次总觉得开发历程中缺点什么。

明年的 hackathon 我们再见～


