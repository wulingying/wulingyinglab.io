---
title: SegmentFault Hackathon 文艺复兴今在沪举行
date: 2016-05-22 16:21:15
tags:
  - hackathon
---
![](https://segmentfault.com/img/bVvR8d)

> 最近一段时间，SegmentFault 的小伙伴们为 Hackathon 文艺复兴都忙坏了，准备好玩的互动环节，采购奇葩的道具，布置比赛场地……今天终于开始了！如果你没有经历过 hackathon 并充满好奇，如果你无法抵达现场而略带遗憾，那就让小编为你带来现场直播。

<!-- more -->

# 赛前准备

![](https://segmentfault.com/img/bVvSbj)
现场布置完毕

![](https://segmentfault.com/img/bVvSaQ)

插排连接完毕

![](https://segmentfault.com/img/bVvR8I)

工作人员就位

![](https://segmentfault.com/img/bVvR8K)

摄影师就位

![](https://segmentfault.com/img/bVvR9u)

主持人 & 同声传译就位（SegmentFault 烧碱 和 AngelHack 亚太区负责人 Ian Chong）

![](https://segmentfault.com/img/bVvR8t)

开发者入座

小编发现，这次 Hackathon，除了 Chinese 开发者，还有歪果仁 coder。初次参与的小伙伴满怀新奇，有经验的都有恃无恐，继续往下看吧。

# Hackathon 开始篇

首先是嘉宾分享，我们邀请来石墨文档创始人陈旭、声网 Agora.io 的首席音视频架构师孙雨润、HPE 汪进以及 2015 Hackathon 杭州站的冠军队代表吕文涛，为大家带来正确打开 Hackathon 的方式。

![](https://segmentfault.com/img/bVvSaX)

*拉斐尔是文艺复兴时期意大利的一位画家、建筑师，是“古典主义艺术”的代表人物。*

在主持人宣布此次 Hackathon 的命题「`拉斐尔：它，或许是某种黑科技，或许是刚刚一念之间的启发，或许像拉斐尔一样汲取达芬奇和米开朗基罗之所长而成就……无论怎样，它代表了你的理想主义，是你的艺术品`」后，大家便开始通过自己的双手、运用编码的力量去实现那个想了很久却一直没有实施的作品了。

# Coding 中的他们

![](https://segmentfault.com/img/bVvSbX)

小编看出了开黑的感觉

![](https://segmentfault.com/img/bVvSbn)

抓到一只歪果仁开发者

![](https://segmentfault.com/img/bVvScm)

占领好位置才能文思泉涌，打码神助

![](https://segmentfault.com/img/bVvScw)

画面让小编想到了传说中的结对编程

![](https://segmentfault.com/img/bVvSqa)

聚精会神

据说就在刚刚，有一群被 OpenCV 玩坏的人们唱起了 high 歌，有目击者提供照片么？

# 番外篇

除了开发日常，我们还为开发者带来了很多别出心裁的道具、零食。

![](https://segmentfault.com/img/bVvSbL)

休息间隙，调戏个机器人也是甚好

![](https://segmentfault.com/img/bVvSb1)

一言不合就敲打字机

![](https://segmentfault.com/img/bVvSb0)

累了？困了？来杯崂山白花蛇草水

![](https://segmentfault.com/img/bVvSbT)

吃完金黄披萨，进入黄金开发

# 深夜篇

夜深了，hackathon 的画风往往会发生变化。

![](https://segmentfault.com/img/bVvSq4)

high 起来的时候，都忘了这是大半夜

![](https://segmentfault.com/img/bVvSrc)

hackathon 三件套：沙发沙发沙发

![](https://segmentfault.com/img/bVvSqc)

闭目小憩中...

![](https://segmentfault.com/img/bVvSrj)

沙发被抢，但休息还要继续(～﹃～)~zZ

关于 coding 的过程，暂且直播到此。对世界充满期待的开发者，会带给我们怎样的艺术作品？且听小编下回直播。

场外的观众们，欢迎来 hackathon 现场感受气氛，为自己喜欢的作品投上一卦 [投票符](http://segmentfault.mikecrm.com/CsgBqi) 吧。