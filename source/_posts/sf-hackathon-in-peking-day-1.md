---
title: Hackathon 文艺复兴北京站 Day 1
date: 2016-06-29 16:21:15
tags:
  - segmentfault
  - hackathon
---
组织 hackathon 文艺复兴的小伙伴带着避暑的心情来到了 33 ℃ 的帝都，33 ℃ 到底能不能让本场 hacakthon 沸腾，请拭目以待吧。

![](https://segmentfault.com/image?src=http://ww3.sinaimg.cn/mw690/795bf814gw1f59wt1vromj24802tcx6u.jpg&objectId=1190000005812366&token=5e7cc35ce52c99d2938c0c8df686233f)

帝都参赛队员和以往有那么一点点不同，这一回全！是！男丁。不过现场还是有妹子的，一个是队友的女票，一个是团队专用鼓励师。额，SegmentFault 小伙伴提前准备的巧克力和撩妹烛光晚餐计划只能就此作罢。罢罢罢，安心搞开发吧！毕竟除了诗和远方，还有眼前的代码。

![](https://segmentfault.com/img/bVyygq)

_石墨文档 CEO 吴冰_

此次 hacakthon，石墨文档 CEO 吴冰出席并参与了演讲，石墨文档作为这一次活动的全程团队协作工具，以「优美协作」在各位开发者心中留下深刻的印象。吴冰也就「为什么要有没的协作」和各位开发者聊起石墨文档的起源，以及成立两年多不忘的初心和信仰。而 Agora 的龚宇华在解说技术支持点之后，又以参赛人员身份组队参与现场 hackathon，并明确表示不参与任何奖品评选，只是单纯地想亲身体验一下 hack 精神。另外，Wacom 的 Sierra 从美国飞来现场，详细讲述 Wacom 的电子墨水技术和应用。
<!-- more -->

![](https://segmentfault.com/image?src=http://ww3.sinaimg.cn/mw690/795bf814gw1f59wslurrej24802tcx6s.jpg&objectId=1190000005812366&token=39d7376426fc7893ef720c88f865ca0b)

Hackathon 北京站，有球赛可看，有代码可撸。

Hackathon 北京站，题为米开朗基罗之王者气息。

Hackathon 北京站，hackathon 专业户再次参与其中。

今天，在北京，就让我们通过 hackathon 的方式，尽情地宣泄这里的人文气息、这里的历史底蕴、这里现代和市井交融的噪音，以及身边那些你可以瞄见的未来。

### 命题

![](https://segmentfault.com/image?src=http://ww2.sinaimg.cn/mw690/795bf814gw1f59wsc2f1aj208i08itbq.jpg&objectId=1190000005812366&token=1135ffd2a147e01542a3d09e6a4ee07a)

_北京站命题：米开朗基罗之王者气息_

米开朗基罗·博那罗蒂(Michelangelo Bounaroti, 1475－1564)，代表了欧洲文艺复兴时期雕塑艺术的最高峰。米开朗基罗的大量作品都奠定在写实的基础上，浸入理想化的渲染，成为当时整个时代的最典型象征。

1505 年，米开朗基罗被迫应邀去画西斯廷教堂天顶壁画。米开朗基罗竟以超凡智慧和毅力，花费四年零五个月的时间，完成了世界上最大的壁画《创世纪》。整幅画通过人与人及人与自然间的关系，歌颂人的创造力及人体美和精神美。

米开朗基罗的作品里所塑造的英雄，既是理想又是现实的反应，充满着动态的韵律。其描绘的巨人气势磅礴，从头到尾都传达着跃动的戏剧性。总之，米开朗基罗的作品常年散发着王者霸气，在力量方面的表现迄今为止无人能及。

