---
title: Week3：张士超，你到底把一周的好文章放哪里了
date: 2016-02-01 16:21:15
tags:
  - frontend
  - css
  - javascript
---
> **错过了一周的优质内容，不要再错过周一的快速回顾**

<!-- more -->

寒潮来得出乎意料，**dong**手**dong**脚，一遍听着史诗巨著《张士超你到底把我家钥匙放哪里了》，一边将优质内容进行整理

## 一周咨询

firim:[《fir.im Weekly - 当技术成为一种 “武器”》](http://segmentfault.com/a/1190000004326043)  
SwiftGG翻译组:[《Swift 一周简讯 #4》](http://segmentfault.com/a/1190000004328525)

## 跟着轮子哥造轮子

vczh:[《GacUI基本概念（二）——排版（2）》](http://segmentfault.com/a/1190000004323573)

## 前端

名一:[《前端远程调试》](http://segmentfault.com/a/1190000004322742)  
德来:[《预加载系列二：让 File Prefetching 丝丝润滑无痛无痒》](http://segmentfault.com/a/1190000004336839)

## 布局

xuehen:《[Flex 布局基础](http://segmentfault.com/a/1190000004320409) 》  
yvonne:《[结合CSS3的布局新特征谈谈常见布局方法](http://segmentfault.com/a/1190000004317590)》

## MySQL

KohPoll:[《关于“时间”的一次探索》](http://segmentfault.com/a/1190000004292140)  
七度尘:[《MySQL存储过程的动态行转列》](http://segmentfault.com/a/1190000004314724)

## JavaScript

caolixiang:[《Angular2 核心概念》](http://segmentfault.com/a/1190000004329594)、[《RxJS 教程》](http://segmentfault.com/a/1190000004293922)  
manxisuo:[《JavaScript：彻底理解同步、异步和事件循环(Event Loop)》](http://segmentfault.com/a/1190000004322358)  
Coding_net:[《理解 Promise 的工作原理》](http://segmentfault.com/a/1190000004325757)  
题叶:[《[译] ClojureScript 中的 JavaScript 互操作》](http://segmentfault.com/a/1190000004315183)  
德来:[《JS 一定要放在 Body 的最底部么？聊聊浏览器的渲染机制》](http://segmentfault.com/a/1190000004292479)

## GNU、XMLHttpRequest、Chrome、Ruby、android

garfileo:[《如何写一个 GNU 风格的命令行程序》](http://segmentfault.com/a/1190000004321899)  
ruoyiqing：[《你真的会使用 XMLHttpRequest吗？》](http://segmentfault.com/a/1190000004322487)  
wilsonliu：[《chrome扩展开发之旅》](http://segmentfault.com/a/1190000004328913)  
Martin91：[《[Ruby on Rails] 嘿，小心你的双等号==》](http://segmentfault.com/a/1190000004319956)  
lauren_liuling：[《EventBus 源码解析》](http://segmentfault.com/a/1190000004295210)

## 程序员访谈

SF 内部访谈:[《程序员的使命是让程序员失业 —— Gemini》](http://segmentfault.com/a/1190000004325426)  
图灵访谈:[ECUG社区发起人许式伟：对编程语言的选择无关阵营，关乎品味](http://segmentfault.com/a/1190000004312606)
