title: uname -a
date: 2016-03-30 00:07:29
---
#### whoami

Ky0ncheng

信仰索尼大法好。 

熱愛開源文化 和 POSIX , 

所以 [GNU/Linux](https://www.gnu.org/gnu/thegnuproject.zh-tw.html) & [UNIX](http://www.unix.org)

業餘 UI/UX Designer.

語言 [zh-wuu](https://zh.wikipedia.org/wiki/%E5%90%B4%E8%AF%AD) 



#### game

[Steam](http://steamcommunity.com/id/ky0ncheng) 

[PSN](https://my.playstation.com/ky0ncheng)

[Origin](https://www.origin.com/en-jp/store/?no-takeover=true) 



#### traceroute

[GDG SHANGHAI](https://plus.google.com/+Gdgshanghai) / [SHLUG](https://twitter.com/shanghailug)

[GitCafe](https://gitcafe.com) -> [Yunpian](https://yunpian.com)


#### mail

[kyoncheng\[at\]yahoo.co.jp](mailto:kyoncheng@yahoo.co.jp)


#### gpg

Key fingerprint = DAE1 D4C7 69B6 3958 94E7 AE8A 2863 401F B70D A4DA

#### bitcoin

1AbVLoqLrouB9HNgPhKbDSmwQXy8pPqaVT

#### git

本博客源代碼托管於 GitCafe 和 [GitHub](https://github.com/ky0ncheng)


#### echo
本博客採用 [署名-非商業性使用-相同方式共享 4.0 協議](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh_TW) 進行許可 
This site is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)
