---
title: 直播技术的云构建技术沙龙
date: 2016-06-24 16:21:15
tags:
  - segmentfault
---
说到直播，相信你一定会想到斗鱼、妹子、RTMP HLS，或者是玩撸啊撸的抠脚大汉……但你一定不会想到阿波罗登月计划。

1969 年，阿波罗 11 号的发射，全世界观看发射现场直播的观众人数高达六亿人，创造了新记录。在当时，记录并向全世界人民直播使用的是慢扫描电视与商业电视的组合——即画面先放映在特殊的显示器上，之后由一台普通电视摄影机对着这台显示器拍摄。尽管这时直播技术落后，视频画质不感人，但确确实实有六亿人共同见证这件意义非凡的大事。

而半个世纪后的今天，静态图片或单向传输早已无法满足大众的娱乐需求。随着互联网技术的发展，云服务商的普及，直播变得大众化，全民直播的热潮一次又一次被掀起。而直播技术的发展，也受到了越来越多开发者的重视，直播体验优劣的最核心技术是什么，各家直播的技术解决方案区别又在哪里？

6 月 25 日，SegmentFault 携手 UCloud 邀请直播行业内各方技术大拿，为大家带来最具实战的直播云构建解决方案。

如何实现存储系统的弹性搭建？
如何优化处理海量的数据存储和请求？
就让我们一起走进 [直播系统的云构](https://segmentfault.com/e/1160000005677298)。

![](https://segmentfault.com/image?src=https://sfault-activity.b0.upaiyun.com/248/613/2486137353-5763617dad004_big&objectId=1190000005793274&token=05fdfc4bb57c98f7421693d0f7396e9c)
<!-- more -->
