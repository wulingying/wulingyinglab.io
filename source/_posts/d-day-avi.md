---
title: D-Day.avi
date: 2016-04-18 16:21:15
tags:
  - segmentfault-d-day
---

一定要科普：`D-Day`的`D`是`Developer`的`D`。

![](https://segmentfault.com/img/bVuXJn)

04-17 摄于杭州

今天太阳高照，心情也是格外棒，穿上 SegmentFault 定制 T ，套上蓝粉相间的阿迪，就这么欢乐地一路飙到了 D-Day 现场。

<!-- more -->

# D-Day 纪录片

![](https://segmentfault.com/img/bVuXNA)

## 11:00

![](https://segmentfault.com/img/bVuXKc)

本次 D-Day 选在属于交通比较方便的一个酒店，<del>（其实杭城到处修路，哪里都堵）</del>，昨日座位已布置完毕。今日，我们只需整好物料，撑好易拉宝，装好资料袋。

# Action

## 13:00

![](https://segmentfault.com/img/bVuXKD)

音响检查完毕，投影仪检查完毕，工作人员就位，摄影师就位。

## 13:30

![](https://segmentfault.com/img/bVuXKp)

小伙伴陆续签到入场，到场的除了大学同学，还有好久不见的钟大师和 Retamia、黑客马拉松上认识的志愿者、传说中的首领 and so on~

> 可爱的依旧可爱，发际线少的只能更少~

可惜传说中的他山之石、Kumfo 竟擦肩而过，搬砖奇竟然放了鸽子，让我感到深深的遗憾，毕竟 D-Day 是个绝好的线下面基的借口。

## 14:00

遗失图片.jpg

大会其实是 1：59 分开始的，集阳光与英俊于一体的烧碱简单介绍了 SegmentFault 后，就是讲师的分享阶段。

![](https://segmentfault.com/img/bVuXMn)

聆听演讲

![](https://segmentfault.com/img/bVuXMe)

分享干货

![](https://segmentfault.com/img/bVuXL2)

提问解疑

ps：具体分享内容隔壁会有官方版，请稍等片刻


## 15:30 

![](https://segmentfault.com/img/bVuXM2)

线下交流

![](https://segmentfault.com/img/bVuXLV)

与讲师咨询问题

![](https://segmentfault.com/img/bVuXLT)

与展位上的合作商交流

茶歇时间，小伙伴们解决三急之后，可以出门呼吸新鲜空气，也可以和讲师面对面探讨技术，和坐在自己周围的小伙伴聊天，亦或是勾搭妹子~

## 16:00

嘉宾继续分享

![](https://segmentfault.com/img/bVuXNM)

阿里来的郑蔚老师和大家一起分享无线技术

![](https://segmentfault.com/img/bVuXNZ)

贝贝的危浩老师讲得入神，台下的观众听得入迷

![](https://segmentfault.com/img/bVuXNT)

台下有很多粉丝的芋头老师

## 18:00

![](https://segmentfault.com/img/bVuXOb)

活动结束，合影留念，在一切排好姿势喊出口号的瞬间被告知这是录像。

`我也爱你们`

## 18:00

太忙没来得及拍片.png
清理场地，散场

## 19:30

其实，这只是一场和平时没有很大区别的 D-Day，但又因类似于「见网友」的面基形式增加了不少神秘感。

一段网络的安全距离，让屏幕前的我们相聊甚欢，但一次小小的活动，又可以让本来熟识的我们，变成了可以一起谈笑风生的朋友。

最后，很开心见了一些我们一直说要约一下，但一直没时间的小伙伴。
很遗憾一直在签到处，只看到大家熟悉的昵称，没有见到诸君本人以及合影留念~

# 一些花絮

![](https://segmentfault.com/img/bVuXMf)

在书上写上活动的纪念文字

![](https://segmentfault.com/img/bVuXFU)

抓拍讲师(｡･∀･)ﾉﾞ

![](https://segmentfault.com/img/bVuXMS)

茶歇时间，妹子将小零食摆好ノ('-'ノ)


