---
title: 我的 21 天阅读分享
date: 2016-03-29 16:21:15
tags:
  - 我的 21 天阅读分享
---

![](https://segmentfault.com/image?src=http://ww2.sinaimg.cn/mw690/795bf814gw1f2clpj7769j20sg0lctaw.jpg&objectId=1190000004711437&token=5dceb767b2e7a65e309096cdc0f7d746)

图片制作：大板栗Clear

[我的 21 天阅读分享](https://segmentfault.com/t/%E6%88%91%E7%9A%84+21+%E5%A4%A9%E9%98%85%E8%AF%BB%E5%88%86%E4%BA%AB/info) 是前端早读课 & SegmentFault 共同推出的活动，治疗拖延，分享知识，认认真真读一本 mark 已久的技术书。

<!-- more -->

# 参与者

以下是活动第一批参与者和他们的阅读分享：

 | 序号 |  sf 用户名  | 	书籍名称	 |  笔记链接  |  完成度 |
 | ------------- |:-------------:| -----:| -----:| -----:|
 | 1 | 	Lxxyx	| 《JavaScript 权威指南》 |	https://segmentfault.com/u/lxxyx/notes	| ✓ |
 | 2 | 	大板栗Clear	| 《React 引领未来的用户界面开发框架》 |	https://segmentfault.com/u/cleardesign/notes	| ✓ |
 | 3 | 	songweiwrite	| 《锋利的jQuery》 |	https://segmentfault.com/u/songweiwrite/notes	| ✓ |
 | 4 | 	crystalbai	| 《阅读 Node.js 源码》 |	https://segmentfault.com/u/crystalbai/notes	| ⍻ |
 | 5 | 	如此	| 《 React 引领未来的用户界面开发框架》 |	https://segmentfault.com/u/ruci/notes	| ⍻ |
 | 6 | 	墨白	| 《JavaScript 权威指南》 |	https://segmentfault.com/u/mobai/notes	| ⍻ |
 | 7 | 	l9m	| 《JavaScript 高级程序设计》 |	https://segmentfault.com/u/l9m/notes	| ✓ |
 | 8 | 	小毛孩	| 《javascript 设计模式与开发实践》 |	https://segmentfault.com/u/xiaomaohai/notes	| ✓ |
 | 9 | 	懒惰的恢	| 《Canvas 基础教程》 |	https://segmentfault.com/u/w406875886/notes	| ⍻ |
 | 10 | 	cyseria	| 《JavaScript 设计模式与开发实践》 |	https://segmentfault.com/u/cyseria/notes	| ✓ |
 | 11 |  gloria_zxr	| 《锋利的 jQuery》 |	https://segmentfault.com/u/gloria_zxr/notes	| ✓ |
 | 12 |  nick_陈	| 《Nod 与 Expressn 开发》 |	https://segmentfault.com/u/nick_chen/notes	| ⍻ |
 | 13 |  我是南南741	| 《jQuery 应用开发实践指南》 |	https://segmentfault.com/u/woshinannan741/notes	| ✓ |
 | 14 |  静如秋叶	| 《JavaScript 高级程序设计》 |	https://segmentfault.com/u/jingruqiuye/notes	| ⍻ |
 | 15 |  吃半个馒头	| 《JavaScript 高级程序设计》 |	https://segmentfault.com/u/qzmly/notes	| ⍻ |
 | 16 |  atiao	| 《Backbone.js 实战》 |	https://segmentfault.com/u/atiao/notes	| ⍻ |
 | 17 |  simmer	| 《JavaScript 设计模式与开发实践》 |	https://segmentfault.com/u/simmer/notes	| ⍻ |
 | 18 |  D哥	| 《JavaScript 设计模式与开发实践》 |	https://segmentfault.com/u/depsi/notes	| ⍻ |
 | 19 |  我是菜鸟	| 《JavaScript 高级程序设计》 |	https://segmentfault.com/u/zxhy/notes	| ✓ |
 | 20	|  eve0803	| 《JavaScript 权威指南》 |	https://segmentfault.com/u/eve0803/notes	| ⍻ |
 | 21 |  biaogebusy	| 《锋利的 jquery》 |	https://segmentfault.com/u/biaogebusy/notes	| ⍻ |

# 活动心得

21 天能改变什么？一起来看看小伙伴的总结吧

伟伟

> 情大的21天活动，我已经参加了两次了，对我的影响也是非常深远
第一次活动开始之前，我对前端只是一个兴趣，没有付诸行动，要知道，单纯的嘴上喜欢并不能持久，之后我接触到了前端早读课，每天都在地铁上看情大写的文章，虽然每一篇文章我并不能有深刻的认识，但我印象里渐渐产生了前端这个工作的雏形，之后参加21天的活动，我认识了好多朋友，他们都有一个共同的特点就是上进心。这不仅带动了我每天的情绪，也让我渐渐感受到了勤奋的力量。

> 这次活动是第二期，我一开始一点信心都没有，因为我技术确实菜的抠脚，然而墨白（活动成员）鼓励我说，其实每个人都是从菜开始的，不可以没有面对的勇气，然后我在地铁上注册了segmentfault账号，报名参加第二次活动，当时点确认报名的时候，我的心情五味陈杂。

> 第二期活动，我学会了用github，学会了markdown，对自己的知识程度更清晰的认识，我在二期活动中遇到了一个朋友叫小静，她是一个非常好问且上进的女生，当时她在群里问github怎么用，我积极地说我会（是真的会），然后小静同学遇到问题就会问我，然后我就百度，解答，从中也获得感悟。我只能说，我的知识水平不够，但我真的很喜欢这种讨论问题的氛围，这种氛围让我上进，让我拓开了视野，就说一个非常简单的例子，二期活动中D哥确实非常厉害，他做事真的很追求完美，github我们大部分成员构建自己的md都是写一段文字，后面是链接，但D哥在他的文章上加上了书的图片，一开始按照书上写例子都是直接敲在文章中，云里雾里，但D哥会用jsbin写成运行程序，之后我也学习他的风格，也学会这么做。假设没有大神带着一起做这么一件有趣的事，那我应该不会要求自己去掌握那么多技能。

cyseria

> 1天阅读分享总结

> 如果说21天养成一个习惯，那@情大和@segment的21天悦读计划更是自我的一个蜕变。本事借着大三课少人闲的心态参加的，没想到最后啃完了一本想了半年都没开始啃的一本书，嗯书还是情大搞活动送的o(*≧▽≦)ツ。

> 先撇开书的具体内容收获的知识，他不仅让自己养成了一个每天看书做笔记的习惯，更有认识一群志同道合的伙伴，虽然不是很熟，但同为一件事一起坚持着也是一件想想就挺有动力的事情。

> 21天期间，也经历了很多事情，跟认识差不多三年的男朋友分手，面了腾讯阿里的实习，因一些事在外面过了几次夜，虽然课程不多，但是看着周围的小伙伴一个两个找到了不错的地方而自己还没什么消息，心里的焦灼怀疑不舍等等都无限次循环回响荡漾。想过几次要放弃，不写了反正也没人认识我，反正也有那么多人没坚持下来，等哪天心情好了我几天就能看完。但或许是骨子里的偏执和不服输每次又咬着牙静下心来，看书，敲案例，提交。又想想一些上班的比自己厉害很多的，都在挤时间为自己充能，想一想还是再努力一会吧，再有每次提交github绿油油的一片真的很有成就感。慢慢的也从最开始拖到晚上开始看书开始写代码到现在甚至有时候可以一起床就开始做事，作息好了，腰也不酸了，拖延症也治得差不多了。就这样，21天竟然在不知不觉中，就那么晃悠着走完了。

> 再有在知识方面，像网上大部分说的一样，前端都是野路子，刷一刷w3cschool，跟着大神写的博客学一些偏方技巧，再自己折腾几个项目，就算出家了，平时买了书形形色色，真正翻完的没有几本，就算完了大多也就囫囵吞枣的， 记住了几个名词甚至过几天这本书讲了什么都不会记得。自己的记性从来都不好，学得快，但所有的知识点不反复几次甚至十几次都是记不得，就像之前看一些东西的api，反复几个月记得了什么函数在什么位置，但是被问道具体细节参数依旧哑口无言只能说看过忘记了。至于这次刚看完看过鹅厂Alloy出品的设计模式，估计有人来问我会不会写什么我还是要去翻一翻，不过大致的思想算是有了一定的了解，对js的面向对象有了一些更深的体会。以后面试起来有人问我闭包问我设计模式估计也没那么方了（吧）。

> 对于前端，有时候也很迷茫，有时候感觉自己正春秋时期百花齐放百家争鸣的时代，几个星期出一个新的库，几个月告诉你原本的XXX有什么不足所以我们应该怎样，道理都懂用起来又是另外一回事。不过，谁让自己当初选择了还爱得无法自拔，也正是这样的挑战性才好玩呐。几年前的自己从没想过会走上代码这样的不归路，不过几年后的现在也从未想过去其他行业扑腾，至少最近几年，毕竟以后的事谁说的准呢。

> 不忘初心，方得始终。无论是这个21天阅读计划，还是今后漫长的路，也慢慢的体会到了一点，所有想要的想做的不去行动不去努力永远也都是一个美好的幻想，比如最基本的，看完一本技术类的书，竟然被拖了半年。觉得自己又要开始深夜心灵鸡汤了，就此打住 (＃°Д°)

> 最后，感谢情大和segment提供的这个平台，也感谢一路大家的陪伴啦啦啦。