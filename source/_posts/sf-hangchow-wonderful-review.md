---
title: SegmentFault 黑客马拉松杭州站精彩回顾
date: 2015-10-30 16:21:15
tags:
  - hackathon
  - segmentfault
---
![](https://segmentfault.com/img/bVqCXm)
  
开发者的世界，颜色总是比平常多一些。[点此也可以观看本次黑客马拉松的视频](http://v.youku.com/v_show/id_XMTM3NzgyMzcxNg==.html?from=s1.8-1-1.2)


## 写在前文前

在微信上给好多杭州团队的小伙伴布置了作业————写一篇这次参加 SegmentFault 举办的黑客马拉松的后感（团队作品技术分享），自我觉悟之后，发现自己不写一个过不去啊，于是便有了此文。

<!--more-->

## 一个非开发者的黑客马拉松日记

> 你又不写代码，干嘛还通宵啊  
> 我就觉着好玩，然后不小心通宵了

## 满血模式

![](https://segmentfault.com/img/bVqCZM)  
灵感一来，文思泉涌，精神劲儿挡都挡不住

![](https://segmentfault.com/img/bVqC9r)  
万能前端技术牛 ls 同学，外加有（友）力队友在，胸有成竹，于是写代码的姿势也变的妖娆，轻松获奖

![](https://segmentfault.com/img/bVqEBR)  
一看队长的衣服，就知道队长是搞技术的

![](https://segmentfault.com/img/bVqDbH)  
你听我的，准没错，准没错，准没错

![](https://segmentfault.com/img/bVqC9i)  
咦，小伙伴们为何笑的如此开心？

![](https://segmentfault.com/img/bVqEBf)  
完全不用担心大家是不是会对打字机感兴趣

![](https://segmentfault.com/img/bVqC9K)  
看这编程节奏，杭电小伙伴要变身农场主了

![](https://segmentfault.com/img/bVqC9l)  
夜深了，小纸条的小伙伴们还在 coding

![](https://segmentfault.com/img/bVqDbG)  
汪汪，身为杭电学姐，也想凑近去听听学弟们讲啥

![](https://segmentfault.com/img/bVqDbI)  
帅气四剑客正在商量怎么把长得像的人聚在一起聊天的软件

![](https://segmentfault.com/img/bVqCdn)  
抓拍之凌晨一点多跟着 大搜狗 团队出去溜达，测试产品性能

![](https://segmentfault.com/img/bVqCXy)  
一不小心，就看到小镇的美

## 休眠模式

![](https://segmentfault.com/img/bVqCYi)  
困了，累了，喝红牛，然并卵  
?隐隐弱弱地问：这莫非就是程序员加班日常？我司程序员从不加班，我不知啊

![](https://segmentfault.com/img/bVqC2e)  
还没开始就犯困了，老子还要再撸一段代码

![](https://segmentfault.com/img/bVqC1V)  
哥程序写完了，终于可以睡觉了，可梦想小镇蚊子真 tm 多。

![](https://segmentfault.com/img/bVqC1s)  
程序终于写的差不多了，有赞小伙伴保持队形，集体趴下歇会儿

![](https://segmentfault.com/img/bVqC6r)  
肿么办，劳资真的写不出来了，求大神附体

![](https://segmentfault.com/img/bVqC9a)  
旁边的哥们都睡了，我两再改改就休息一下吧

![](https://segmentfault.com/img/bVqC89)  
队友们都趴下了，我还在忘我修改中

![](https://segmentfault.com/img/bVqDbJ)  
最后，说好的不搞基呢，求真相？

## 身为程序猿的工作人员乱入

![](https://segmentfault.com/img/bVqCXK)  
前一秒：可以躺下来休息一下了，椅子真硌人

![](https://segmentfault.com/img/bVqCV7)  
后一秒：不行不行，我该该醒醒去改改 bug 了

## 写在后感后

1、第一次尝试用 markdown 语法来写文章，有不好不能说不好，哼哼。  
2、感谢摄影师，感谢志愿者，感谢开发者  
3、马拉松没做啥大事，就给大家搬搬红牛，分分夜宵零食，但很哈皮  
4、最后，周末想用 replay 弄一段视频

