---
title: 👍 SegmentFault Hackathon 文艺复兴深圳站获奖作品简介
date: 2016-06-05 16:21:15
tags:
  - hackathon
---
> Hackathon 是一门艺术，自由遐想，任意发挥；hackathon 也是一项竞技，限时开发，挑战极限。

![](https://segmentfault.com/img/bVxTvW)

本次深圳站共有 13 支团队，最终产生 11 个成型作品。开发者紧贴深圳站命题——[列奥纳多·达·芬奇](https://segmentfault.com/a/1190000005656846#articleHeader0)，并结合实际，利用代码和技术端口去解决身边的问题，发现日常的趣味，下面将为大家带来本次 hacakthon 获奖作品的简单介绍。

<!-- more -->


![](https://segmentfault.com/img/bVxTCW)  
_Ruff 人气奖兼 Agora 单项奖：Cavaliers 团队，作品：蒙娜丽莎的喜怒哀乐_

Cavaliers 团队基于`文艺复兴时期，虽然动乱，但是掩盖不住天才的光芒。和平年代，看似平静，但是遮掩不了内心的波澜`的想法，为大家带来了作品——**蒙娜丽莎的喜怒哀乐**：当你参与到聊天中，作品就可以检测你的面部，为你戴上蒙娜丽莎的面具，但是没有人可以将心藏起，一旦你的情绪波动较大时，系统就会自动向对方发送一条消息，告诉他/她你的心情。

![](https://segmentfault.com/img/bVxTHh)  
_SegmentFault Hackathon 深圳站第三名：New Bee 团队_

经过长久的头脑风暴，团队小伙伴最终决定围绕「境外游」来实现功能，虽然还没有给这个作品取好名字，但作品主要是面向境外游的伙伴，解决语言不通的问题，主要功能是机器人翻译和人工服务翻译 2 种方式。

![](https://segmentfault.com/img/bVxTG6)  
_SegmentFault Hackathon 深圳站第二名，石墨文档单项奖获得者：Zero 团队，作品：FaceBattle_

Zero 团队由四位热爱极客及工匠精神，希望用情怀改变世界的大学生组成，为大家带来了作品 FaceBattle，顾名思义就是用自己的表情来作为对战的媒介，通过抓取面部表情并分析喜悦哀怒值，在分数上进行 PK。


`据说团队为做测试，几个男孩自拍了四百余张（(´◉‿ゝ◉｀)` 

![](https://segmentfault.com/img/bVxTHF)  
_SegmentFault Hackathon 深圳站第一名：hACKbUSTER 团队，作品：Project DaVinci_

名为「Project DaVinci」是一个让没有移动 App 开发技能却又有此需求的人（比如你家老板或 PM）轻松地制作出属于自己的 App 的工具，非常契合本次 hackathon 的主题 "Anyone Can Code"。

你一定没有猜到，团队中的两位，是 SegmentFault Hackathon 2015 北京站的第一名。


`所以小编弱弱地问，hACKbUSTER 是 hACK老司机 的意思么(　˙灬˙　)` 

介绍完这些作品，小编才恍然发现比赛已过去一天多了，但脑海里还不时回响着某个小伙伴大半夜的飘摇呼喊：“醒醒，代码还没改完呢”，牢记着一条中肯的建议「下次 hackathon 能搞点面膜吗」。

这次 SegmentFault Hackathon 深圳站结束的有点仓促，都没来得及和小伙伴好好道个别，亢奋的同时不免心生曲终人散的惆怅。这次 hackathon，应该是小编经历的多场 hackathon 中最放心的一场，这里不光要感谢四位密谋明年继续参与的超能志愿者，还要感谢群里为小伙伴指引比赛地点的热心参赛者。

## 命题

![](https://segmentfault.com/img/bVxTxd)  
_列奥纳多·达·芬奇 / Leonardo da Vinci_

> “我突然产生了两种情绪——害怕和渴望：对漆黑的洞穴感到害怕，又想看看其中是否会有什么怪异的东西。”

列奥纳多·达·芬奇，他的一生都被这样的情绪所左右，一方面是对生活的不可知和神秘感到害怕，而另一方面又不惜一切想把这种神秘加以研究和揭露。这是一颗创造的心灵，是一个有着不可遏制的好奇心和极其活跃的想象力的人。

达芬奇是文艺复兴时期人文主义的代表，集科学理性和艺术感性于一身。在人文艺术、生物、科学等领域都有显著的发明创造成就。我们从他的各类作品和笔记中发现他的人格和思想，他做前人未曾设想，科技结合人文，发明和创造。

在接下来的 30 小时内，让我们重拾自己的画笔——我们的代码，去探索身边的未知，去发现和创造，完成属于自己的作品，美好一点这个世界！

## more

每场 hackahton，[石墨文档](https://shimo.im/) 和 声网[Agora.io](http://cn.agora.io/) 都深度参与其中，为各位开发者在现场文档实时协作及音视频相关需求上提供支持。

另外，[SegmentFault Hackathon 北京站](https://segmentfault.com/e/1160000005060768)将在 6 月 25-26 日举办，欢迎热爱挑战的小伙伴加入其中。