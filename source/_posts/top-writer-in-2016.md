---
title: SegmentFault 2016 第一季度 Top Writer
date: 2016-05-25 16:21:15
tags:
  - writer
---

> 他们不光为 SegmentFault 社区做了巨大贡献，也为更多开发者搭起向上攀登的阶梯。

[SegmentFault Top Writer](https://segmentfault.com/t/segmentfault-top-writer/info) 是 SegmentFault 社区里一群卓越的开发者，除了大量产出优秀技术文章，他们还热心帮助大家解决开发过程中遇到的问题。在他们当中，有些是全栈工程师，有些精通某一开发领域，有些擅长撰写技术文章，提供解决方案，有些热衷答疑解惑，帮助新手入门。

![](https://segmentfault.com/img/bVwCof)

2016 年第一季度，我们分别在问答、文章两方面各筛选出 15 位用户，并送上专门设计的 Top Writer T 恤、Top Writer 鼠标垫及 Top Writer 贴纸，感谢他们一直以来的支持。 

<!-- more -->

🌺 恭喜 [hsfzxjy](https://segmentfault.com/u/hsfzxjy) 荣登本季问答文章双榜

问答

优秀问答作者 | 声望增长值 | 附 1 个高票回答
----|------|----
有明 | 7781 | 第三方登录，多登录方式的回调地址问题
代码宇宙 | 4449 | 关于javascript中的new
manxisuo | 4173 | JavaScript既然是单线程的，那么异步要怎么理解？
hsfzxjy | 4130 | 为什么有些网站的页面地址，没有加上html或者php或者jsp的后辍名？
kikong | 4118 | 编写add函数 然后 add(1)(2)(3)(4) 输出10 再考虑拓展性
chuyao | 2529 | 为什么android开发一般不new一个Activity或者Service呢？
jokester | 2008 | 关于一个js闭包里的作用域链的问题
ty0716 | 1985 | 关于php 实例一个类的一点问题
边城 | 1938 | 自己写的CSS与使用的框架冲突如何解决呢？
zonxin | 1879 | 关于function中的this指向问题
jaege | 1865 | c++为什么只能输出一次
leveychen | 1836 | 隐藏System.out.println
Eapen | 1463 | php提取这段字符串中间的一段内容
外籍杰克 | 1450 | element.innerHTML和appendChild有什么区别
codepiano | 1436 | 程序面试算法问题
SamirChen | 1344 | iPad 应用支持全方法，想让某个ViewController在某些场景下不能旋转

文章

优秀文章作者 | 声望增长值 | 附 1 篇高票文章
----|------|----
jimmy_thr | 2385 | 《前端通信进阶》
neu | 1535 | 《Android工具箱之Android 6.0权限管理》
JerryC | 1019 | 《如果你用GitHub，可以这样提高效率》
啃先生 | 684 | 【前端构建】WebPack实例与前端性能优化
zach5078 | 654 | 《30分钟掌握ES6/ES2015核心内容（上）》
德来 | 570 | 《JS 一定要放在 Body 的最底部么？聊聊浏览器的渲染机制》
stormzhang | 527 | 《吐血推荐珍藏的Chrome插件》
HarryZhu | 450 | 《解密Airbnb数据流编程神器：Caravel 颠覆许多大数据分析平台的开源工具》
ruoyiqing | 450 | 《你真的会使用 XMLHttpRequest 吗？》
SwiftGG翻译组 | 419 | 《使用 Realm 和 Swift 创建 ToDo 应用》
山河永寂 | 415 | 《单刷 APUE 系列丨第一章 - Unix基础知识》
杨川宝 | 408 | 《vue 源码分析之如何实现 observer 和 watcher》
jeffjade | 384 | 《如何优雅地使用 Sublime Text》
hsfzxjy | 362 | 《17 行代码实现的简易 Javascript 字符串模板》
王下邀月熊_Chevalier | 362 | 《我的前端之路》
andyyu0920 | 359 | 2015 的工具包，持續邁向 2016！
garfileo | 341 | 《这么多年，差不多一直在为自己写程序》


注：声望增长值为用户在当季产生内容的声望增长




