---
title: Week4 优质文章整理
date: 2016-02-01 16:21:15
tags:
  - android
  - php
  - javascript
---

> **错过了一周的优质内容，不要再错过周一的快速回顾**

<!-- more -->

## 一周

fir.im Weekly -[《深度揭秘 App 启动全过程》](http://segmentfault.com/a/1190000004365768)  
SwiftGG翻译组 -[《每周 Swift 社区问答》](http://segmentfault.com/a/1190000004369765)

## Ruby 入门

![](https://segmentfault.com/img/bVsCiW)  
chenge3k:[《Ruby 语言简明入门与提高》](http://segmentfault.com/a/1190000004349506)

## 一只爬虫

![](https://segmentfault.com/img/bVsCiy)  
崔小拽:[《php 爬虫：知乎用户数据爬取和分析》](http://segmentfault.com/a/1190000004357994)

## Android

![](https://segmentfault.com/img/bVsCiP)  
li21:[《如何学习 Android Animation》](http://segmentfault.com/a/1190000004354609)  
NsstringFromName:[《Animations 开源动效分析（二）POP-Stroke 动画》](http://segmentfault.com/a/1190000004365988)

neu:[《Android最佳实践（一）》](http://segmentfault.com/a/1190000004357190)  
[《Android 工具箱之 Activity 生命周期》](http://segmentfault.com/a/1190000004328796)  
[《Android 工具箱之 Android 6.0 权限管理》](http://segmentfault.com/a/1190000004348012)

## iOS

![](https://segmentfault.com/img/bVsCiS)  
BigNer:[《一步步创建自己的iOS框架》](http://segmentfault.com/a/1190000004328931)  
iOS程序犭袁:[《iOS 网络缓存扫盲篇 - 使用两行代码就能完成 80% 的缓存需求》](http://segmentfault.com/a/1190000004356632)

## JS

cnsnake11:[《ReactNative 增量升级方案》](http://segmentfault.com/a/1190000004352162)  
caolixiang:[《RxJS API解析（四）》](http://segmentfault.com/a/1190000004350904)  
xiaoyu2er:[《从 JavaScript 继承说起, 深入理解 Angular Scope 继承关系》](http://segmentfault.com/a/1190000004358393)

### ES6

![](https://segmentfault.com/img/bVsCjh)  
zach5078:[《30 分钟掌握 ES6/ES2015 核心内容（上）》](http://segmentfault.com/a/1190000004365693)  
长颈鹿:[《实现简易 ES6 Promise 功能（二）》](http://segmentfault.com/a/1190000004358563)

## 其他：

杨川宝:[《解析神奇的 Object.defineProperty》](http://segmentfault.com/a/1190000004346467)  
chenjd:[《为什么 DirectX 里表示三维坐标要建一个 4*4 的矩阵？》](http://segmentfault.com/a/1190000004361135)  
sumory:[《基于 OpenResty 的 Lua Web 框架 lor0.0.2 预览版发布》](http://segmentfault.com/a/1190000004349554)

## SF 内部访谈

SF.GG:[《用匠心和铁手，打造自己的人生 —— Wtser》](http://segmentfault.com/a/1190000004330622)  
SF.GG:[《Codes Don't Lie —— Integ》](http://segmentfault.com/a/1190000004330616)

