---
title: 一场站着听完的 D-Day——广州站「前端移动端」专场回顾
date: 2016-06-29 16:21:15
tags:
  - segmentfault
  - hackathon
---
D-Day 2016 马上要进入尾声了，这一次分享，广州靓仔靓妹的好学，对技术的热情，都超乎了小编想象。

![](https://ssl.daoapp.io/ww3.sinaimg.cn/mw690/795bf814gw1f535jm4ln4j21dc0ww1hf.jpg)

_组队而来的开发者_

![](https://ssl.daoapp.io/ww4.sinaimg.cn/mw690/795bf814gw1f535ipesdqj21dc0ww1kx.jpg)

_好学的开发者 1_

![](https://ssl.daoapp.io/ww1.sinaimg.cn/mw690/795bf814gw1f535ji9hpvj21dc0wwnno.jpg)

_好学的开发者 2_

![](https://ssl.daoapp.io/ww3.sinaimg.cn/mw690/795bf814gw1f535jtczwyj21dc0wwx54.jpg)

_好学的开发者 3_

![](https://ssl.daoapp.io/ww4.sinaimg.cn/mw690/795bf814gw1f535j4zmx0j21dc0wwh9a.jpg)

_站立听讲的开发者 1_

![](https://ssl.daoapp.io/ww4.sinaimg.cn/mw690/795bf814gw1f535ilcz3vj21dc0wwham.jpg)

_站立听讲的开发者 2_

![](https://ssl.daoapp.io/ww3.sinaimg.cn/mw690/795bf814gw1f535jw1fupj21dc0wwqoh.jpg)

_站立听讲的开发者 3_


那么广州站的讲师们都带来了什么分享，跟着小编来瞅瞅吧

## 嘉宾分享
![](https://ssl.daoapp.io/ww1.sinaimg.cn/mw690/795bf814gw1f538dc1pcvj21dc0wwx4d.jpg)

_魅族前端开发李志伟，议题《Flyme hybird 优化实践》_

首位分享嘉宾是魅族前端开发李志伟，带来了以下内容：

*   hybrid 离线方案

*   在线和离线工程化方案

*   面向组件编程

更多内容请戳[《Flyme hybird 优化实践》](http://pan.baidu.com/s/1dE4HfkH)

![](https://ssl.daoapp.io/ww1.sinaimg.cn/mw690/795bf814gw1f535jesqkzj21dc0wwe4d.jpg)

_微信前端工程师杜光敏，议题《用 Electron 打造跨平台的前端工具 App》_

接下来，来自腾讯的微信前端工程师杜光敏带来了议题[《用 Electron 打造跨平台的前端工具 App》](http://pan.baidu.com/s/1o8OXJuU)。Electron 作为 Github 官方的大作，一经发布就广受关注，让前端工程师也能快速开发桌面 App 成为可能。讲师分享前端工具 WeFlow 的开发历程，并带你领略 Electron 的魅力。

![](https://ssl.daoapp.io/ww3.sinaimg.cn/mw690/795bf814gw1f535j8mx94j21dc0wwtw4.jpg)

_微店技术专家林阳，议题《Components Cloud Practices in IOING Framework》_

茶歇过后，微店技术专家林阳讲师和大家全面解析云组件化的实现原理，更多相关内容，请戳→ [《Components Cloud Practices in IOING Framework》](http://pan.baidu.com/s/1pLEMvZt)。

![](https://ssl.daoapp.io/ww1.sinaimg.cn/mw690/795bf814gw1f535iu7gzqj21dc0ww1i7.jpg)

_阿里巴巴移动事业群高级前端工程师刘欣然，议题《Vue 性能优化在最佳实践》_

如何把性能提升了300%？如何解决长列表的内存泄漏？美女讲师——阿里巴巴移动事业群高级前端工程师刘欣然紧贴 UC 业务场景，分享了[《Vue 性能优化在最佳实践》](http://pan.baidu.com/s/1o8QZEzg)。

![](https://ssl.daoapp.io/ww1.sinaimg.cn/mw690/795bf814gw1f535ixzpp9j21dc0ww4mr.jpg)

_阿里巴巴技术专家翁叔，议题《阿里巴巴 Push 发展之路》_

Agoo 是阿里巴巴手淘团队构建的高性能、高可用 Push 平台，支撑了双11中手机淘宝等海量 app 的 push 需求。这是一条怎样的 push 之路，请听最后一位分享嘉宾——阿里巴巴技术专家翁叔讲[《阿里巴巴Push发展之路》](http://pan.baidu.com/s/1jHAC52Q)。

### 活动分享

##### 以下是讲师分享的干货汇总：

*   李志伟 -[《Flyme hybird 优化实践》](http://pan.baidu.com/s/1dE4HfkH)

*   杜光敏 -[《用 Electron 打造跨平台的前端工具 App》](http://pan.baidu.com/s/1o8OXJuU)

*   林阳 -[《Components Cloud Practices in IOING Framework》](http://pan.baidu.com/s/1pLEMvZt)

*   刘欣然 -[《Vue 性能优化在最佳实践》](http://pan.baidu.com/s/1o8QZEzg)

*   翁叔 -[《阿里巴巴Push发展之路》](http://pan.baidu.com/s/1jHAC52Q)

##### [现场照片分享](http://pan.baidu.com/s/1jIybKo)

### More about SegmentFault D-Day

结束了广州场，D-Day 将会出发到大连，大连站的主题是云服务专场，希望给大连地区开发者们带来惊喜👉 [D-Day 大连站](https://segmentfault.com/e/1160000005059284)。

SegmentFault D-Day，全称 SegmentFault Developer Day，是由 SegmentFault 发起主办的系列技术沙⻰。自 2014 年启动，D-Day 技术沙龙已在北、上、广、深、杭等 10 个城市巡回举办了 19 场，涉及前端、云计算、后端、大数据、移动开发等众多主题，活动覆盖数千位开发者。

2016 年，SegmentFault 将继续立足社区，传播前沿的技术实践内容：今年 5 个月，D-Day 将跨越 10 个城市，邀请重量级嘉宾，贴合业务需求，精选分享议题，为开发者打造更具价值的线下技术交流。

<!-- more -->
