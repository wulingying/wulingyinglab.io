---
title: SF Week 2：本周好内容，请君过目
date: 2016-01-16 16:21:15
tags:
  - html
  - php
  - java
  - javascript
  - c
  - sublime-text
---
> **错过了一周的优质内容，不要再错过周一的快速回顾**

<!-- more -->

### SF 产品 news

Noodles [《SegmentFault 热门内容优化》](http://segmentfault.com/a/1190000004253816)

### 系列文章

neu [《Gradle for Android 第一篇( 从 Gradle 和 AS 开始 )》](http://segmentfault.com/a/1190000004229002)及系列文章  
牧曦之晨 [《[译]GC专家系列1：理解Java垃圾回收》](http://segmentfault.com/a/1190000004233812)  
牧曦之晨 [《[译]GC专家系列2：Java 垃圾回收的监控》](http://segmentfault.com/a/1190000004255118)

### CSS

EdwardUp [《3d transform 的坐标空间及位置》](http://segmentfault.com/a/1190000004233074)  
array_huang [《巧用 margin/padding 的百分比值实现高度自适应（多用于占位，避免闪烁）》](http://segmentfault.com/a/1190000004231995)

### Java 和 JavaScript 没有一点关系

beanlam [《Java IO : RandomAccessFile》](http://segmentfault.com/a/1190000004247899)  
颜海镜 [《不可错过的 javascript 迷你库》](http://segmentfault.com/a/1190000004236363)  
animabear [《简单的 js 异步文件加载器 》](http://segmentfault.com/a/1190000004227850)

### 工具

名一 [《Fiddler 实用教程》](http://segmentfault.com/a/1190000004240812)  
jeffjade [《如何优雅地使用 Sublime Text》](http://segmentfault.com/a/1190000004248611)

### 其他

吴隐隐 [《用脚本快速查看自己被多少微信好友删除》](http://segmentfault.com/a/1190000004251795)  
garfileo [《接口即泛型》](http://segmentfault.com/a/1190000004250754)  
chenjd [《趣说游戏 AI 开发：对状态机的褒扬和批判》](http://segmentfault.com/a/1190000004241958)  
leozdgao [《探索 React 源码的全局模块系统》](http://segmentfault.com/a/1190000004238404)
