---
title: SegmentFault Hackathon 文艺复兴深圳站简讯
date: 2016-06-04 16:21:15
tags:
  - hackathon
---
![](https://ssl.daoapp.io/ww4.sinaimg.cn/mw690/795bf814gw1f4jkft24v6j24n4334he0.jpg)

[Hackathon 上海站](https://segmentfault.com/a/1190000005174175)结束，SegmentFault Hackathon 文艺复兴又来到了新兴之城深圳，且跟着小编一起到现场 see see (∩❛ڡ❛∩)

<!-- more -->

![](https://ssl.daoapp.io/ww3.sinaimg.cn/mw690/795bf814gw1f4jk9t7zedj24n4334x6w.jpg)

![](https://ssl.daoapp.io/ww4.sinaimg.cn/mw690/795bf814gw1f4jkag7mtpj24n43347wp.jpg)

![](https://ssl.daoapp.io/ww3.sinaimg.cn/mw690/795bf814gw1f4jkb5d4hgj24n43347wq.jpg)

首先要介绍下我们深圳站的参赛者，有从广州、佛山、香港坐车过来的，也有从北京打飞的过来的。参赛成员中最小的是 98 年的，最大的……额，最大的小编也不知道(*ゝω・)只知道有位工作了超过 8 年，看上去约摸四十岁的叔告诉小编说：「我的梦想就是一直编码下去」。

此外，SegmentFault Hackathon 1024 北京站 top1 团队和 AngelHack Hackathon 香港站 top1 团队也来到了深圳参赛。

![](https://ssl.daoapp.io/ww3.sinaimg.cn/mw690/795bf814gw1f4jkf8lmmej22o01i0x6q.jpg)

![](https://ssl.daoapp.io/ww3.sinaimg.cn/mw690/795bf814gw1f4jkg00pxtj22o01i0hdu.jpg)

介绍完成员，我们再来讲讲 opening 环节——API 宣讲。Agora 将上海站 Top1 作品 ——Debattle——同时也是 agora 单项奖作品，作为实际案例分享，让大家更详细地了解并使用之。此处夸一下深圳站参赛者小伙伴的英文水平，全程无需翻译，只在遇到「搞基」、「入坑」等俚语时卡顿 1s。

![](https://ssl.daoapp.io/ww4.sinaimg.cn/mw690/795bf814gw1f4jke6ax6wj24n4334he3.jpg)

![](https://ssl.daoapp.io/ww2.sinaimg.cn/mw690/795bf814gw1f4jkdbi6rij24n4334kjw.jpg)

结束完严谨的 opening 环节，马上进入欢乐的介绍环节。自带喜剧效果的小伙伴介绍了自己的团队，环着萌萌哒颈枕小哥拖着行李箱来晒装备，某队长说参加 hackathon 的原因是家里空调坏了……（咳咳

![](https://ssl.daoapp.io/ww2.sinaimg.cn/mw690/795bf814gw1f4jkca4alej22o01i0x6q.jpg)

![](https://sf-static.b0.upaiyun.com/v-5763d879/global/img/squares.svg)

在团队欢乐逗逼介绍中，我们也拥有感动。这次 hackathon 中，我们终于听到了一直想要听到的声音——back to the original hackathon！SegmentFault Hackathon 文艺复兴，就是要激起 hacker 本身的艺术气息，回归到最初的 hackathon，正如 hackathon 爱好者陈雨恒所说的，坚持做最纯粹的 hackathon！

![](https://sf-static.b0.upaiyun.com/v-5763d879/global/img/squares.svg)

![](https://sf-static.b0.upaiyun.com/v-5763d879/global/img/squares.svg)

![](https://sf-static.b0.upaiyun.com/v-5763d879/global/img/squares.svg)

在 SegmentFault Hackathon 文艺复兴深圳站，我们见到了小伙伴的热情与优雅。他们高效地组队、就位；他们有节奏地开发、休息；他们沉浸于编码、调试，全然没有发现深圳骤降的暴雨。明天，他们会带着怎样的作品出现？小编也和你一样，期待见到暴风雨过后惊艳的天空。

最后，如果你觉着 hackathon 文艺复兴好玩，并想加入其中，那么，就不要错过今年的最后一场哈——[SegmentFault Hackathon 文艺复兴北京站](https://segmentfault.com/e/1160000005060768)(〃−｀ω´−)