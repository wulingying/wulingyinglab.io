---
title: 修改 Markdown.here 基本渲染 CSS ，一键搞定微信公众平台文章编辑
date: 2015-12-25 16:21:15
tags:
  - 微信公众平台
  - markdown
---
和程序猿呆久了之后的一个表现就是：不想机械复制粘贴，如果能够搞几行代码，或者运行几个脚本，真是甚好。本文的部分方法，还是诸位程序猿教的O(∩_∩)O。

## Why Markdown

首先，Markdown 大法好。

其次，因为想到大家在这里写文章都用 `markdown`，妹纸就想着，微信公众平台上的文章用 `markdown`格式是不是更有利于我们程序猿哥哥阅读呢（别夸我贴心😊我是来求关注的

## Next

打开 Chrome ，搜**关键词**：微信、markdown

万能的某乎上找到了这么一个话题「[如何排版微信公众平台的文章？](https://www.zhihu.com/question/23640203)」。

下载了 [Markdown.here](https://chrome.google.com/webstore/detail/markdown-here/elifhakcjgalahccnjkneoccemfahfoa) 插件进行编辑测试。

<!-- more -->

## 测试报告

将文章复制到微信后台，快捷键 Ctrl+Alt+M，Markdown 格式就一键搞定了，赛高。但是针对手机端的屏幕，显示就出现以下问题：

1.  **字体**： 一级标题和正文文字都偏大 2 px

2.  **代码颜色太单调**：代码前加了高亮标识，但因没注明是 javascript/ruby or other languages，全部都是黑色的，阅读体验很不好

3.  类似于「值的类型 `type`」中 type 无法用红色标出来

所以一阵高兴之后妹纸的内心又奔溃了，本来就是为了偷懒找了插件，但出现上述问题之后，又要进行手动复制粘贴，不开森，于是又墨迹了一会儿，竟发现 Markdown.here 插件竟然可以修改。

**插件修改路径：** Chrome → 设置 → 扩展程序 → markdown.here → 选项 → 基本渲染 CSS

## 解决方式

### 问题1：字体大小

首先修改的是标题大小，分别将一级标题修改为 20 px；二级标题修改为 18 px

```css
h1, h2, h3, h4, h5, h6 {
  margin: 1.3em 0 1em;
  padding: 0;
  font-weight: bold;
}

h1 {
  font-size: 20px;
  border-bottom: 1px solid #ddd;
}

h2 {
  font-size: 18px;
  border-bottom: 1px solid #eee;
}

h3 {
  font-size: 16px;
}

h4 {
  font-size: 16px;
}

h5 {
  font-size: 16px;
}
```

但是想到正文内容应该修改为 14 px；又在内容最后加了一句，当然这也是某位程序猿哥哥告诉我的：

```css
p {
  font-size: 14px;
}；
```

一番修改之后，仍出现以下问题：

*   之后的文字，依旧是 16 px；

妹子又开始骚扰某涛程序员，得到的解决方法是在 `li` 后面加入 font-size ：

```css
li {
  margin: 0.5em 0;
  font-size: 14px;
}；
```

终于完美解决了问题 1

### 问题2：语法高亮

接下来解决代码高亮问题，虽然选择了语法高亮的主题，但是复制到微信平台之后，全是黑色的，和小蚂蚁一样，代码阅读体验非常不好，改了几个主题之后，还是不行，内心有 1W 头草泥马奔过，后来在研究基本渲染 CSS 的时候，发现解决方案很简单：

在三个 ``` 之后加上 「javascript」或者「ruby」或者「其他语言」即可，原本黑色的代码，就会变成下图所示：

![](https://sf-static.b0.upaiyun.com/v-573ed5d9/global/img/squares.svg)

### 问题3：「值的类型 `type`」中 type 无法用红色标出来

这个问题研究了一天，终于被傻傻的我搞定了，直接在「基本渲染 CSS」中加上这一句即可

```css
p code {
  color: rgb(217, 33, 66);
};
```

授人与鱼不如授人以渔，下图为诸位提供查找途径，这样子以后遇到类似的事情，就都可以轻松解决了~

**路径：**找到这一东西对应的元素，发现是在 `p code`中的 `color`，然后在 CSS 中添加语句。

![](https://sf-static.b0.upaiyun.com/v-573ed5d9/global/img/squares.svg)

## 有感而发

写了这么一大段教程，有些语法可能还不是很好= =，但却是让工作变得更便捷。  
但其实最简单的解决途径是：找个程序员男票😓

## 附

妹子其实还遇到一个问题，就是选择用空 4 格高亮代码的，在微信上生成 markdown 之后，代码是无法高亮的，请问这个该如何解决？需要加个判断么？

**以下为「基本渲染 CSS」初始化内容**

```css
/*
 * NOTE:
 * - The use of browser-specific styles (-moz-, -webkit-) should be avoided.
 *   If used, they may not render correctly for people reading the email in
 *   a different browser than the one from which the email was sent.
 * - The use of state-dependent styles (like a:hover) don't work because they
 *   don't match at the time the styles are made explicit. (In email, styles
 *   must be explicitly applied to all elements -- stylesheets get stripped.)
 */

/* This is the overall wrapper, it should be treated as the `body` section. */
.markdown-here-wrapper {
}

/* To add site specific rules, you can use the `data-md-url` attribute that we
   add to the wrapper element. Note that rules like this are used depending
   on the URL you're *sending* from, not the URL where the recipient views it.
*/
/* .markdown-here-wrapper[data-md-url*="mail.yahoo."] ul { color: red; } */

pre, code {
  font-size: 0.85em;
  font-family: Consolas, Inconsolata, Courier, monospace;
}

code {
  margin: 0 0.15em;
  padding: 0 0.3em;
  white-space: pre-wrap;
  border: 1px solid #EAEAEA;
  background-color: #F8F8F8;
  border-radius: 3px;
  display: inline; /* added to fix Yahoo block display of inline code */
}

pre {
  font-size: 1em;
  line-height: 1.2em;
}

pre code {
  white-space: pre;
  overflow: auto; /* fixes issue #70: Firefox/Thunderbird: Code blocks with horizontal scroll would have bad background colour */
  border-radius: 3px;
  border: 1px solid #CCC;
  padding: 0.5em 0.7em;
  display: block !important; /* added to counteract the Yahoo-specific `code` rule; without this, code blocks in Blogger are broken */
}

/* In edit mode, Wordpress uses a `* { font: ...;} rule+style that makes highlighted
code look non-monospace. This rule will override it. */
.markdown-here-wrapper[data-md-url*="wordpress."] code span {
  font: inherit;
}

/* Wordpress adds a grey background to `pre` elements that doesn't go well with
our syntax highlighting. */
.markdown-here-wrapper[data-md-url*="wordpress."] pre {
  background-color: transparent;
}

/* This spacing has been tweaked to closely match Gmail+Chrome "paragraph" (two line breaks) spacing.
Note that we only use a top margin and not a bottom margin -- this prevents the
"blank line" look at the top of the email (issue #243).
*/
p {
  /* !important is needed here because Hotmail/Outlook.com uses !important to
     kill the margin in <p>. We need this to win. */
  margin: 0 0 1.2em 0 !important;
}

table, pre, dl, blockquote, q, ul, ol {
  margin: 1.2em 0;
}

ul, ol {
  padding-left: 2em;
}

li {
  margin: 0.5em 0;
}

/* Space paragraphs in a list the same as the list itself. */
li p {
  /* Needs !important to override rule above. */
  margin: 0.5em 0 !important;
}

/* Smaller spacing for sub-lists */
ul ul, ul ol, ol ul, ol ol {
  margin: 0;
  padding-left: 1em;
}

/* Use Roman numerals for sub-ordered-lists. (Like Github.) */
ol ol, ul ol {
  list-style-type: lower-roman;
}

/* Use letters for sub-sub-ordered lists. (Like Github.) */
ul ul ol, ul ol ol, ol ul ol, ol ol ol {
  list-style-type: lower-alpha;
}

dl {
  padding: 0;
}

dl dt {
  font-size: 1em;
  font-weight: bold;
  font-style: italic;
}

dl dd {
  margin: 0 0 1em;
  padding: 0 1em;
}

blockquote, q {
  border-left: 4px solid #DDD;
  padding: 0 1em;
  color: #777;
  quotes: none;
}

blockquote::before, blockquote::after, q::before, q::after {
  content: none;
}

h1, h2, h3, h4, h5, h6 {
  margin: 1.3em 0 1em;
  padding: 0;
  font-weight: bold;
}

h1 {
  font-size: 1.6em;
  border-bottom: 1px solid #ddd;
}

h2 {
  font-size: 1.4em;
  border-bottom: 1px solid #eee;
}

h3 {
  font-size: 1.3em;
}

h4 {
  font-size: 1.2em;
}

h5 {
  font-size: 1em;
}

h6 {
  font-size: 1em;
  color: #777;
}

table {
  padding: 0;
  border-collapse: collapse;
  border-spacing: 0;
  font-size: 1em;
  font: inherit;
  border: 0;
}

tbody {
  margin: 0;
  padding: 0;
  border: 0;
}

table tr {
  border: 0;
  border-top: 1px solid #CCC;
  background-color: white;
  margin: 0;
  padding: 0;
}

table tr:nth-child(2n) {
  background-color: #F8F8F8;
}

table tr th, table tr td {
  font-size: 1em;
  border: 1px solid #CCC;
  margin: 0;
  padding: 0.5em 1em;
}

table tr th {
 font-weight: bold;
  background-color: #F0F0F0;
}
```

## last, but not the least

谢谢阅读，以后可以在 SegmentFault 微信留言，教妹子编(xue)微(dai)信(ma)哦，这样子就会有更多好玩的功能和大家互动了。

![](https://sf-static.b0.upaiyun.com/v-573ed5d9/global/img/squares.svg)
