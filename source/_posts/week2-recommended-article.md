---
title: SF Week 2：本周好内容，请君过目
date: 2016-01-16 16:21:15
tags:
  - html
  - php
  - java
  - javascript
---
> **错过了一周的优质内容，不要再错过周一的快速回顾**

<!-- more -->

## 好玩的标题和实在的内容

chenjd：[《谁偷了我的热更新？Mono，JIT，iOS》](http://segmentfault.com/a/1190000004248070)  
andyyu0920：[《從<琅琊榜>學 Redux》](http://segmentfault.com/a/1190000004257207)  
jimmy_thr：[《拖动中的味道》](http://segmentfault.com/a/1190000004259725)

## Javascript 标签下

dmyang：[《Hybrid APP 架构设计思路》](http://segmentfault.com/a/1190000004263182)  
andyyu0920：[《詳解 ES6 Unicode》](http://segmentfault.com/a/1190000004271352)  
Amo[《Serverless! 使用 AWS 開發 Slack Slash Commands》](http://segmentfault.com/a/1190000004282941)

## 其他

然则：[《Java StringBuilder 和 StringBuffer 源码分析》](http://segmentfault.com/a/1190000004261063)  
Lxxyx：[《HTML meta 标签总结与属性使用介绍》](http://segmentfault.com/a/1190000004279791)  
quietin：[《python 单例模式与 metaclass》](http://segmentfault.com/a/1190000004278703)  
技术人攻略：[《2016，买一台更强大的空气净化器，还是翻墙看看外面的世界？》](http://segmentfault.com/a/1190000004255210)

## 系列文章上新了

飞狐：[《听飞狐聊 JavaScript 设计模式系列 14》](http://segmentfault.com/a/1190000004270004)  
beanlam：[《Java IO : 流，以及装饰器模式在其上的运用》](http://segmentfault.com/a/1190000004255439)  
zchq88：[《Webpack+Angular 的项目 SEED 下》](http://segmentfault.com/a/1190000004265119)  
neu：[《Gradle for Android 第六篇( 测试)》](http://segmentfault.com/a/1190000004260141)、[《Gradle for Android 第七篇( Groovy入门 )》](http://segmentfault.com/a/1190000004276167)

